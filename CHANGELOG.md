# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to
[Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Support @renato-wiki/core@^0.4.0
- Pass `req` property to `"theme::view:render"` hook

## [0.1.2] - 2017-05-28

### Added
- Support @renato-wiki/core@^0.3.0

## [0.1.1] - 2017-05-28

### Fixed
- Compiled files not being served to npm

## [0.1.0] - 2017-05-28 [YANKED]

### Added
- Create plain search plugin
