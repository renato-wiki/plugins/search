/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const fs = require("fs");
const lunr = require("lunr");
const Promise = require("bluebird");

const fsReadFile = Promise.promisify(fs.readFile);

/*===================================================== Exports  =====================================================*/

module.exports = search;

/*==================================================== Functions  ====================================================*/

function search(renato, query, offset, amount) {
  let builder = createBuilder();
  let index = renato.index;
  let promises = [];
  applyRecursive(renato, promises, builder, index);
  return Promise
      .all(promises)
      .then(() => builder.build().search(query))
      .then((results) => {
        let result = {query, offset, amount, total: results.length};
        result.entries = [];
        return Promise
            .map(results.splice(offset, amount), _.partial(addSearchResult, result))
            .then(_.constant(result));
      });

  function addSearchResult(result, searchResult) {
    return renato.apiData
        .getContentData(searchResult.ref.split("/"))
        .then((contentData) => {
          if (contentData.index == null) { return; }
          let indexData = contentData.index[renato.indexDataKey];
          let entry = {index: indexData.index.flat, score: searchResult.score};
          let parents = [];
          while ((indexData = indexData.parent) != null && indexData.parent != null) {
            parents.push(indexData.index.flat);
          }
          entry.parentPath = _.reverse(parents);
          result.entries.push(entry);
        });
  }
}

function applyRecursive(renato, promises, builder, index) {
  if (index.type === "page") {
    promises.push(
        fsReadFile(index[renato.indexDataKey].path)
            .then((content) => builder.add({id: index.uri, title: index.meta.title, content}))
    );
  }
  if (index.children != null) {
    let pages = index.children.pages, categories = index.children.categories;
    for (let i = 0; i < pages.length; i++) { applyRecursive(renato, promises, builder, pages[i]); }
    for (let i = 0; i < categories.length; i++) { applyRecursive(renato, promises, builder, categories[i]); }
  }
}

function createBuilder() {
  const builder = new lunr.Builder();
  builder.pipeline.add(lunr.trimmer);
  builder.field("title");
  builder.field("content");
  return builder;
}
