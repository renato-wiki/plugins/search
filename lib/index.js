/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");

const Plugin = require("./Plugin");

const VIEWS = ["search"];

/*===================================================== Exports  =====================================================*/

module.exports = use;

/*==================================================== Functions  ====================================================*/

function use(renato, config) {
  let plugin = new Plugin(renato, config);
  renato.addHandler("core::routes:bind", plugin.bindRoutes.bind(plugin));
  renato.addHandler("theme::view:compile", plugin.addNavigation.bind(plugin));
  _.each(VIEWS, (view) => {
    renato.addHandler("theme::view:read:" + view, plugin.readViewPre.bind(plugin, view), -1);
    renato.addHandler("theme::view:read:" + view, plugin.readView.bind(plugin, view), 1);
  });
  return plugin;
}
