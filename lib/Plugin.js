/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/* eslint no-sync:off */
"use strict";

const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");
const serveStatic = require("serve-static");

const search = require("./search");

const fsReadFile = Promise.promisify(fs.readFile);

const BASE_DIR = path.join(__dirname, "..");
const STYLES_DIR = path.join(BASE_DIR, "target", "css");
const SCRIPTS_DIR = path.join(BASE_DIR, "target", "scripts");
const TEMPLATES_DIR = path.join(BASE_DIR, "templates");
const SNIPPET_SEARCH_BAR = fs.readFileSync(path.join(TEMPLATES_DIR, "snippets", "search-bar.pug"));

/*===================================================== Exports  =====================================================*/

module.exports = Plugin;

/*==================================================== Functions  ====================================================*/

function Plugin(renato, config) {
  this.renato = renato;
  this.config = config;
  this.cache = renato.debug ? null : {};
}

Plugin.prototype.bindRoutes = function (routers) {
  if (routers.ui != null) {
    let serveOptions = {cacheControl: !this.renato.debug, index: false};
    routers.ui.get("/search", this.routeSearch.bind(this));
    routers.ui.use("/css/plugins/plugin-search", serveStatic(STYLES_DIR, serveOptions));
    routers.ui.use("/scripts/plugins/plugin-search", serveStatic(SCRIPTS_DIR, serveOptions));
  }
  if (routers.api != null) {
    routers.api.get("/search", this.routeSearchData.bind(this));
  }
};

Plugin.prototype.addNavigation = function (data) {
  if (data.content != null && data.content.pug != null) { data.content.pug += SNIPPET_SEARCH_BAR; }
  return data;
};

Plugin.prototype.readViewPre = function (view, data) { /* {view, options, content} */
  data.file = path.join(TEMPLATES_DIR, "views", view + ".pug");
  return data;
};

Plugin.prototype.readView = function (view, data) { /* {view, options, content} */
  return fsReadFile(data.file)
      .then((pugSource) => {
        data.content.pug = pugSource;
        return data;
      });
};

Plugin.prototype.routeSearchData = function (req, res) {
  this._searchLocals(req)
      .then((locals) => res.send(locals));
};

Plugin.prototype.routeSearch = function (req, res, next) {
  this._searchLocals(req)
      .then((locals) => this._renderSearch(locals, req, res, next));
};

Plugin.prototype._searchLocals = function (req) {
  let query = req.query.q || "";
  let offset = +req.query.offset || 0;
  if (offset < 0) { offset = 0; }
  if (query == null || query.trim().length < 3) {
    return Promise.resolve({query, offset, error: "Invalid query. The query needs to be at least 3 chars long."});
  } else {
    return search(this.renato, query, offset, 20);
  }
};

Plugin.prototype._renderSearch = function (locals, req, res, next) {
  this.renato
      .hookReduce("theme::view:render", {view: "search", locals, req, res})
      .catch(next);
};
