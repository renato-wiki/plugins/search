/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
document.addEventListener("DOMContentLoaded", function () {
  "use strict";

  var BLACKLIST = ["INPUT", "SELECT", "TEXTAREA"];
  var searchBar = document.getElementById("header-search");

  document.addEventListener("keydown", function (event) {
    if (event.which === 27 /* escape */) { event.target.blur(); }
  });

  document.addEventListener("keypress", function (event) {
    if (event.which === 47) {
      var nodeName = event.target.nodeName;
      for (var i = 0; i < BLACKLIST.length; i++) { if (BLACKLIST[i] === nodeName) { return; } }
      searchBar.focus();
      event.preventDefault();
    }
  });

});
